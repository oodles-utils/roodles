#' Vector-safe min(1, max(0, x))
#'
#' vectorized function using pmax/pmin to constrain numeric x to [0, 1] whereas
#' min(1, max(0,
#' x)) would return a scalar
#'
#' @param x a vector for which comparisons with numeric 1, 0 are valid, namely
#' `numeric`, `integer` and `logical`.
#' @return x constrained to [0, 1]
#' @export
to_zero_one <- function(x) {
  pmin(1, pmax(0, x))
}

#' log sum exponential
#'
#' R has log1p but not logsumexp
#'
#' @param x a vector for which `exp` and `max` are defined
#' @return numeric vector
#' @export
logsumexp <- function(x) {
  m <- max(x)
  y <- exp(x - m)

  log(sum(y)) + m
}
